package laterunner.core;

import java.util.LinkedList;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import laterunner.model.collisions.BorderHitEvent;
import laterunner.model.collisions.ObstacleHitEvent;
import laterunner.model.collisions.WorldEvent;
import laterunner.model.collisions.WorldEventListener;
import laterunner.model.saving.FileManager;
import laterunner.model.world.GameState;
import laterunner.model.world.GameStateImpl;
import laterunner.model.world.World;
import laterunner.graphics.Menu;
import laterunner.graphics.Scene;
import laterunner.graphics.SceneImpl;
import laterunner.input.Command;
import laterunner.input.Controller;

/**
 * Game's engine.
 */
public class GameEngine implements Controller, WorldEventListener {

    private static final long PERIOD = 16; /* 16 ms = 60 frame per second */
    private GameState gameState;
    private Scene view;
    private boolean commandList = false;
    private BlockingQueue<Command> commandQueue;
    private LinkedList <WorldEvent> eventQueue;
    private int levelNumb;
    private boolean isSurvival = false;

    /**
     * Instantiates game engine.
     */
    public GameEngine() {
        commandQueue = new ArrayBlockingQueue<>(100);
        eventQueue = new LinkedList<>();
    }

    /**
     * Loads main settings and shows main menu.
     */
    public void gameInit() {
        FileManager.loadFromFile();
        view = new SceneImpl(this, this);
    }

    /**
     * Loads level.
     * 
     * @param levelNumber
     *          level's number
     * @param score
     *          level's score
     */
    public void setupLevel(final int levelNumber, final int score) {
        this.commandQueue.clear();
        this.levelNumb = levelNumber;
        if (levelNumber > 10 && !(this.isSurvival)) {
            this.isSurvival = true;
            this.levelNumb = 1;
        } else if (this.levelNumb > 10) {
            this.levelNumb = 10;
        }
        this.gameState = new GameStateImpl(this, this.levelNumb, score);
        this.view.getRoad().setGameState(this.gameState);
    }

    /**
     * The loop which manages the gameplay.
     */
    public void mainLoop() {
        this.commandList = true;
        view.getRoad().getAudio().play();
        long lastTime = System.currentTimeMillis();
        while (!gameState.isLevelFinished() && !this.gameState.isEndSurvival()) {
            long current = System.currentTimeMillis();
            int elapsed = (int) (current - lastTime);
            processInput();
            updateGame(elapsed);
            render();
            waitForNextFrame(current);
            lastTime = current;
        }
        this.gameState.updateStats();
        endLevel();
    }

    private void endLevel() {
        if (!this.isSurvival) {
            this.commandList = false;
            view.getRoad().getAudio().stop();
            SceneImpl.changePanel("menu");
            Menu.updateLevel();
        } else {
            this.setupLevel(++this.levelNumb, this.gameState.getScore());
            new Thread(this.view.getRoad()).start();
        }
    }

    /**
     * Makes the main loop sleeping.
     * 
     * @param current
     *          time used to calculate how much to waits
     */
    private void waitForNextFrame(final long current) {
        long dt = System.currentTimeMillis() - current;
        if (dt < PERIOD) {
            try {
                Thread.sleep(PERIOD - dt);
            } catch (Exception ignored) { }
        }
    }

    private void processInput() {
        Command cmd = commandQueue.poll();
        if (cmd != null) {
            cmd.execute(this.gameState);
        }
    }

    private void updateGame(final int elapsed) {
        this.gameState.update(elapsed);
        checkEvents();
    }

    private void checkEvents() {
        World w = gameState.getWorld();
        for (WorldEvent ev : eventQueue) {
            if (ev instanceof ObstacleHitEvent) {
                ObstacleHitEvent cev = (ObstacleHitEvent) ev;
                if (this.isSurvival) {
                    this.gameState.setEndSurvival(true);
                    this.isSurvival = false;
                } else {
                    w.removeObstacle(cev.getCollisionObj());
                    gameState.decrementScore(cev.getCollisionObj());
                }
            } else if (ev instanceof BorderHitEvent) {
                gameState.decrementScoreByBorder();
                }
            }
            if (eventQueue.isEmpty()) {
                gameState.incrementScore(2);
            }
            eventQueue.clear();
    }

    private void render() {
        view.render();
    }

    /**
     * Adds the command to the command queue.
     * 
     * @param command
     *          Command executed
     */
    public void notifyCommand(final Command command) {
        if (this.commandList) {
            commandQueue.add(command);
        }
    }

    /**
     * Returns the command queue.
     * 
     * @return
     *          command queue
     */
    public BlockingQueue<Command> getCommandQueue() {
        return this.commandQueue;
    }

    /**
     * Adds the event to the event queue.
     * 
     * @param event
     *          Event happened
     */
    public void notifyEvent(final WorldEvent event) {
        eventQueue.add(event);
    }

    /**
     * Gets survival mode.
     * 
     * @return
     *          true if survival.
     */
    public boolean isSurvival() {
        return isSurvival;
    }
}

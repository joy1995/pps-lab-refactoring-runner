package laterunner.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import laterunner.model.vehicle.UserVehicle;
import laterunner.model.vehicle.Vehicle;
import laterunner.model.vehicle.VehicleTypes;
import org.junit.Test;

import laterunner.model.vehicle.Obstacle;
import laterunner.physics.P2d;
import laterunner.physics.S2d;

/**
 * Tests Vehicle's method.
 */
public class VehicleTest {

    private static final int X_POS = 435;
    private static final int Y_POS = 510;
    private static final P2d INITIAL_USER_POSITION = new P2d(435, 510);
    private static final S2d INITIAL_USER_SPEED = new S2d(0, 0);
    private static final int BUS_MAL = 3000;

    /**
     * Tests Vehicle's initial fields.
     */
    @Test
    public void testInitialFields() {
        Vehicle car = new UserVehicle(VehicleTypes.BUS, INITIAL_USER_POSITION, INITIAL_USER_SPEED);
        P2d pos = new P2d(X_POS, Y_POS);
        assertTrue(car.getCurrentPosition().getX() == (pos.getX())
                && car.getCurrentPosition().getY() == pos.getY());
        Obstacle ob = new Obstacle(VehicleTypes.BUS, new P2d(X_POS, 1),
                new S2d(0, 1));
        assertEquals(ob.getLifeDamage(), 3);
        assertEquals(ob.getMalus(), BUS_MAL);
        car.setSpeed(new S2d(1, 1));
        assertTrue(car.getCurrentSpeed().getX() == 1
                && car.getCurrentSpeed().getY() == 0);
        car.setSpeed(new S2d(1, 1));
        assertTrue(ob.getCurrentSpeed().getX() == 0
                && ob.getCurrentSpeed().getY() == 1);
        assertEquals(car.getType(), VehicleTypes.USER_CAR);
        assertEquals(ob.getType(), VehicleTypes.BUS);
    }
}

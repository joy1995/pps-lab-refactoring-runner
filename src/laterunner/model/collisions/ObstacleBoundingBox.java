package laterunner.model.collisions;

import laterunner.core.Dimensions;
import laterunner.model.vehicle.Vehicle;
import laterunner.model.vehicle.VehicleTypes;

/**
 * Horizontal segment of the obstacles' occupied space.
 */
public class ObstacleBoundingBox implements BoundingBox {

    private double xPos;
    private double xPosSim;

    /**
     * Instantiates the horizontal segment of the obstacle's occupied space.
     * 
     * @param posX
     *          left side of the obstacle
     * @param obstacle
     *          type of the obstacle
     */
    public ObstacleBoundingBox(final double posX, final VehicleTypes obstacle) {
        this.xPos = posX;
        this.xPosSim = this.xPos + Dimensions.getDimensions().getVehicleWidth(obstacle);
    }

    @Override
    public boolean isCollidingWith(final Vehicle vehicle) {
        return vehicle.getCurrentPosition().getX() <= xPosSim
                && (vehicle.getCurrentPosition().getX() + Dimensions.getDimensions().getVehicleWidth(vehicle.getType())) >= this.xPos;
    }

}

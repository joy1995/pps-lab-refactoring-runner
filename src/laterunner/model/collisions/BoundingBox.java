package laterunner.model.collisions;

import laterunner.model.vehicle.Vehicle;

/**
 * Object's box used to manage collisions.
 */
public interface BoundingBox {

    /**
     * Checks if an item is colliding with user's car.
     * 
     * @param vehicle
     *          user's car
     * @return
     *          true if there is a collision
     */
    boolean isCollidingWith(final Vehicle vehicle);
}

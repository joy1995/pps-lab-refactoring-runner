package laterunner.model.world;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import laterunner.core.Dimensions;
import laterunner.model.collisions.BorderBoundingBox;
import laterunner.model.collisions.BorderHitEvent;
import laterunner.model.collisions.ObstacleHitEvent;
import laterunner.model.collisions.WorldEventListener;
import laterunner.model.level.SetLevel;
import laterunner.model.vehicle.Obstacle;
import laterunner.model.vehicle.UserVehicle;
import laterunner.model.vehicle.Vehicle;
import laterunner.model.vehicle.VehicleTypes;
import laterunner.physics.P2d;
import laterunner.physics.S2d;

/**
 * The class in witch is implemented the world features.
 *
 */
public class WorldImpl implements World {

    private static final int LOWER_BORDER = 720;
    private static final int BORDER_DAMAGE = 500;
    private static final P2d INITIAL_USER_POSITION = new P2d(435, 510);
    private static final S2d INITIAL_USER_SPEED = new S2d(0, 0);

    private List<Obstacle> obstacles;
    private SetLevel setLevel;
    private Vehicle userVehicle;
    private BorderBoundingBox mainBorderBoundingBox;
    private Optional<WorldEventListener> worldEventListener;

    /**
     * 
     * @param borderBoundingBox
     *          the bounding box to be set
     */
    WorldImpl(final BorderBoundingBox borderBoundingBox) {
        this.obstacles = new LinkedList<>();
        this.setLevel = new SetLevel();
        this.mainBorderBoundingBox = borderBoundingBox;
        this.worldEventListener = Optional.empty();
    }

    @Override
    public void setEventListener(final WorldEventListener worldEventListener) {
        this.worldEventListener = Optional.of(worldEventListener);
    }

    @Override
    public void removeObstacle(final Obstacle obstacle) {
        this.obstacles.remove(obstacle);
    }

    @Override
    public List<Vehicle> getSceneEntities() {
        List<Vehicle> entities = new ArrayList<>(obstacles);
        entities.add(userVehicle);
        return entities;
    }

    @Override
    public void updateState(final int elapsed) {
        for (Obstacle obstacle : obstacles) {
            obstacle.updateState(elapsed);
            if (obstacle.getCurrentPosition().getY() > LOWER_BORDER) {
                obstacles.remove(obstacle);
            }
        }
        userVehicle.updateState(elapsed);
        checkBoundaries();
        checkCollisions();
    }

   @Override
   public void generateLevel(final int i) {
        this.setUserVehicle(new UserVehicle(VehicleTypes.USER_CAR, INITIAL_USER_POSITION, INITIAL_USER_SPEED));
        this.obstacles.addAll((this.setLevel.getLevel(i)).getLevel());
    }

    @Override
    public Vehicle getUserVehicle() {
        return userVehicle;
    }

    @Override
    public int getBorderDamage() {
        return BORDER_DAMAGE;
    }

    private void setUserVehicle(final Vehicle c) {
        this.userVehicle = c;
    }

    private void checkBoundaries() {
        if (this.mainBorderBoundingBox.isCollidingWith(getUserVehicle())) {
                worldEventListener.ifPresent(e -> e.notifyEvent(new BorderHitEvent()));
        }
    }

    private void checkCollisions() {
        Optional<Obstacle> found = Optional.empty();
        for (Obstacle obstacle: obstacles) {
            if (checkUserPosition(obstacle) && !found.isPresent()) {
                if (obstacle.getBBox().isCollidingWith(getUserVehicle())) {
                    found = Optional.of(obstacle);
                }
            }
        }
        found.ifPresent(obstacle -> worldEventListener.ifPresent(e -> e.notifyEvent(new ObstacleHitEvent(obstacle))));
    }

    private boolean checkUserPosition(Obstacle obstacle) {
        return obstacle.getCurrentPosition().getY() + Dimensions.getDimensions().getVehicleHeight(obstacle.getType()) >= (userVehicle.getCurrentPosition().getY() + 2)
                && obstacle.getCurrentPosition().getY() <= userVehicle.getCurrentPosition().getY() + Dimensions.getDimensions().getVehicleHeight(userVehicle.getType());
    }

}

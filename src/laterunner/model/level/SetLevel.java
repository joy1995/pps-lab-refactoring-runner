package laterunner.model.level;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import laterunner.model.vehicle.VehicleTypes;
import laterunner.physics.S2d;

/**
 * The class that set the levels' features.
 *
 */
public class SetLevel {

    private final CreateLevel creator;
    private final List<VehicleTypes> list;
    private final Random random;

    /**
     * The standard constructor.
     */
    public SetLevel() {
        this.creator = new CreateLevelImpl();
        this.list = new LinkedList<>();
        this.random = new Random();
    }

    /**
     * Based on the level create the level.
     * 
     * @param levelNumber
     *          the level to be played
     *
     * @return
     *          the level
     */
    public Level getLevel(final int levelNumber) {
        switch(levelNumber) {
            case 1: return this.setLevelFeatures(300, 90, 50, levelNumber);
            case 2: return this.setLevelFeatures(350, 75, 70, levelNumber);
            case 3: return this.setLevelFeatures(400, 60, 90, levelNumber);
            case 4: return this.setLevelFeatures(450, 50, 110, levelNumber);
            case 5: return this.setLevelFeatures(500, 40, 120, levelNumber);
            case 6: return this.setLevelFeatures(550, 40, 130, levelNumber);
            case 7: return this.setLevelFeatures(600, 30, 140, levelNumber);
            case 8: return this.setLevelFeatures(650, 20, 150, levelNumber);
            case 9: return this.setLevelFeatures(700, 10, 175, levelNumber);
            case 10: return this.setLevelFeatures(800, 1, 200, levelNumber);
            default: throw new IllegalStateException();
        }
    }

    private Level setLevelFeatures(final int obstacleSpeed, final int obstacleDistance, int numberCars, final int levelNumber) {
        S2d speed = new S2d(0, obstacleSpeed);
        while (numberCars > 0) {
            list.add(selectVehicle(levelNumber));
            numberCars--;
        }
        return creator.generateLevel(list, speed, obstacleDistance);
    }

    private VehicleTypes selectVehicle(final int levelNumber) {
        final double rateTruck = 0.03;
        final double rateCar = 0.1;
        double rand = this.random.nextDouble();
        VehicleTypes obstacle;

        if (this.random.nextDouble() <= rateTruck * levelNumber) {
            obstacle = VehicleTypes.BUS;
        } else if (rand > rateTruck * levelNumber && rand <= rateCar * levelNumber) {
            obstacle = VehicleTypes.OBSTACLE_CAR;
        } else {
            obstacle = VehicleTypes.MOTORBIKE;
        }
        return obstacle;
    }

}

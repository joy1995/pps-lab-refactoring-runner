package laterunner.model.vehicle;

import laterunner.model.collisions.BoundingBox;
import laterunner.model.collisions.ObstacleBoundingBox;
import laterunner.physics.P2d;
import laterunner.physics.S2d;

/**
 * Game obstacle.
 */
public class Obstacle extends Vehicle {

    private static final int CAR_MALUS = 1000;
    private static final int CAR_LIFE_DAMAGE = 1;
    private static final int BUS_MALUS = 3000;
    private static final int BUS_LIFE_DAMAGE = 3;
    private static final int MOTORBIKE_MALUS = 2000;
    private static final int MOTORBIKE_LIFE_DAMAGE = 0;

    private int malus;
    private int lifeDamage;
    private BoundingBox bBox;

    /**
     * Istantiates a new Obstacle.
     * 
     * @param vehicleType
     *          obstacle's type
     * @param position
     *          obstacle starting position
     * @param speed
     *          obstacle starting speed
     */
    public Obstacle(final VehicleTypes vehicleType, final P2d position, final S2d speed) {
        super(vehicleType, position, speed);

        switch(vehicleType) {
        case OBSTACLE_CAR:
            this.malus = CAR_MALUS;
            this.lifeDamage = CAR_LIFE_DAMAGE;
            break;
        case BUS:
            this.malus = BUS_MALUS;
            this.lifeDamage = BUS_LIFE_DAMAGE;
            break;
        case MOTORBIKE:
            this.malus = MOTORBIKE_MALUS;
            this.lifeDamage = MOTORBIKE_LIFE_DAMAGE;
            break;
        default:
            throw new IllegalArgumentException("Obstacle with user car feature; nice try.");
        }

        this.bBox = new ObstacleBoundingBox(this.getCurrentPosition().getX(), vehicleType);
    }

    @Override
    public boolean checkSpeed(S2d speed) {
        return speed.getX() > 0;
    }

    /**
     * Returns obstacle's malus.
     * 
     * @return
     *          obstacle's malus
     */
    public int getMalus() {
        return this.malus;
    }

    /**
     * Returns obstacle's life damage.
     * 
     * @return
     *          obstacle's life damage
     */
    public int getLifeDamage() {
        return this.lifeDamage;
    }

    /**
     * Returns obstacle's bounding box.
     * 
     * @return
     *          obstacle's bounding box
     */
    public BoundingBox getBBox() {
        return this.bBox;
    }


}

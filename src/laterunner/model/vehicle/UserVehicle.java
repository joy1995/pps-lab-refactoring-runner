package laterunner.model.vehicle;

import laterunner.physics.P2d;
import laterunner.physics.S2d;

/**
 * User's Car.
 */
public class UserVehicle extends Vehicle {

    /**
     * Instantiates a new Car.
     */
    public UserVehicle(VehicleTypes vehicleType, final P2d position, final S2d speed) {
        super(vehicleType, position, speed);
    }

    @Override
    public boolean checkSpeed(S2d speed) {
        return speed.getY() > 0;
    }
}

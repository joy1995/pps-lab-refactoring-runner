package laterunner.model.vehicle;

import laterunner.core.Dimensions;
import laterunner.model.collisions.BorderBoundingBox;
import laterunner.physics.P2d;
import laterunner.physics.S2d;

/**
 * Template of a vehicle.
 */
public abstract class Vehicle {

    private static final double UPDATING_CONSTANT = 0.001;

    private P2d position;
    private S2d speed;
    private VehicleTypes type;

    /**
     * VehicleTypes' main constructor.
     * 
     * @param position
     *          vechicle's initial position
     * @param speed
     *          vehicleType's initial speed
     * @param vehicleType
     *          vehicleType's type
     */
    public Vehicle(final VehicleTypes vehicleType, final P2d position, final S2d speed) {
        if ((position.getX() < BorderBoundingBox.getBorderBoundingBox().getUpperLeft().getX()
                    && position.getX() + Dimensions.getDimensions().getVehicleWidth(vehicleType)
                    > BorderBoundingBox.getBorderBoundingBox().getBelowRight().getX())
                || (vehicleType == VehicleTypes.USER_CAR && speed.getY() != 0)
                || (vehicleType != VehicleTypes.USER_CAR && speed.getX() != 0)) {
            throw new IllegalArgumentException();
        } else {
            this.position = position;
            this.speed = speed;
            this.type = vehicleType;
        }
    }

    /**
     * Sets vehicle's position.
     * 
     * @param position
     *          vehicle's new position
     */
    public void setPosition(final P2d position) {
        if (position.getX() < BorderBoundingBox.getBorderBoundingBox().getUpperLeft().getX()
                    && position.getX() + Dimensions.getDimensions().getVehicleWidth(this.getType())
                    > BorderBoundingBox.getBorderBoundingBox().getBelowRight().getX()) {
            throw new IllegalArgumentException();
        } else {
            this.position = position;
        }
    }

    /**
     * Recalls the checking setter.
     *
     * @param speed
     *          unchecked vehicle's new speed
     */
    public void setSpeed(S2d speed) {
        if(checkSpeed(speed)) {
            throw new IllegalArgumentException();
        }
        setCheckedSpeed(speed);
    }

    /**
     * Calculates new vehicle's position depending on the speed.
     * 
     * @param elapsed
     *          time elapsed between two frames
     */
    public void updateState(final int elapsed) {
        this.position = this.position.sum(speed.mul(UPDATING_CONSTANT * elapsed));
    }

    /**
     * Returns vehicle's current position.
     * 
     * @return
     *          vehicle's current position
     */
    public P2d getCurrentPosition() {
        return this.position;
    }

    /**
     * Returns vehicle's current speed.
     * 
     * @return
     *          vehicle's current speed
     */
    public S2d getCurrentSpeed() {
        return this.speed;
    }

    /**
     * Returns vehicle's type.
     * 
     * @return
     *          vehicle's type
     */
    public VehicleTypes getType() {
        return this.type;
    }

    /**
     * check if speed is correct.
     * @param speed the param to check
     * @return if is correct
     */
    protected abstract boolean checkSpeed(final S2d speed);

    /**
     * Sets the vehicles's speed after checking it.
     *
     * @param speed
     *          checked vehicle's new speed
     */
    private void setCheckedSpeed(final S2d speed) {
        this.speed = speed;
    }

}

package laterunner.model.saving;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Array;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import laterunner.model.shop.Shop;
import laterunner.model.user.User;

/**
 * Class which contains functions to save and load user's info.
 */
public final class FileManager {

    private static final String FILE = "file_manager.txt";

    private FileManager() { }

    /**
     * Saves user's info from file.
     */
    public static void saveToFile() {
        try (BufferedWriter w = Files.newBufferedWriter(Paths.get(FILE))) {
            List<String> toFile = getFieldsList();
            Iterator<String> it = toFile.iterator();
            while (it.hasNext()) {
                w.write(it.next());
                it.remove();
                w.newLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Loads user's info from file.
     */
    public static void loadFromFile() {
        List<String> fromFile;
        try (BufferedReader br = Files.newBufferedReader(Paths.get(FILE))) {
            fromFile = br.lines().collect(Collectors.toList());
            setFieldsList(fromFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static List<String> getFieldsList() {
        return Arrays.asList(String.valueOf(User.getUser().getMoney()),
                String.valueOf(User.getUser().getUserLives()),
                String.valueOf(User.getUser().getLevelReached()),
                String.valueOf(User.getUser().getSpeedMultiplier()),
                String.valueOf(User.Statistic.getStatistic().getGamesPlayed()),
                String.valueOf(User.Statistic.getStatistic().getLostLives()),
                String.valueOf(User.Statistic.getStatistic().getSurvivalHighScore()),
                String.valueOf(User.Statistic.getStatistic().getMotorbikeHits()),
                String.valueOf(User.Statistic.getStatistic().getObstacleCarHits()),
                String.valueOf(User.Statistic.getStatistic().getTruckHits()),
                String.valueOf(Shop.getShop().getLifeCost()),
                String.valueOf(Shop.getShop().getSpeedCost()));
    }

    private static void setFieldsList(final List<String> list) {
        Iterator<String> it = list.iterator();
        User.getUser().setMoney(Integer.parseInt(it.next()));
        it.remove();
        User.getUser().setUserLives(Integer.parseInt(it.next()));
        it.remove();
        User.getUser().setLevelReached(Integer.parseInt(it.next()));
        it.remove();
        User.getUser().setSpeedMultiplier(Double.parseDouble(it.next()));
        it.remove();
        User.Statistic.getStatistic().setGamesPlayed(Long.parseLong(it.next()));
        it.remove();
        User.Statistic.getStatistic().setLostLives(Long.parseLong(it.next()));
        it.remove();
        User.Statistic.getStatistic().setSurvivalHighScore(Long.parseLong(it.next()));
        it.remove();
        User.Statistic.getStatistic().setMotorbikeHits(Long.parseLong(it.next()));
        it.remove();
        User.Statistic.getStatistic().setObstacleCarHits(Long.parseLong(it.next()));
        it.remove();
        User.Statistic.getStatistic().setTruckHits(Long.parseLong(it.next()));
        it.remove();
        Shop.getShop().setLifeCost(Integer.parseInt(it.next()));
        it.remove();
        Shop.getShop().setSpeedCost(Integer.parseInt(it.next()));
        it.remove();
    }
}

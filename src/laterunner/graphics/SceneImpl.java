package laterunner.graphics;

import laterunner.core.GameEngine;
import laterunner.input.Controller;
import laterunner.input.MoveLeft;
import laterunner.input.MoveRight;
import laterunner.input.Stop;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.*;

/**
 * Scene Implementation.
 *
 */
public class SceneImpl extends JFrame implements Scene, KeyListener {

    private static final long serialVersionUID = 1L;
    private static final int WIDTH = 960;
    private static final int HEIGHT = 720;
    private static CardLayout cl = new CardLayout();
    private static JPanel content = new JPanel(cl);
    private Road road;
    private Controller controller;

    /**
     * Common Scene Constructor: instantiates all the panels and set Menu as first view.
     * 
     * @param gmEngine
     *          instance of Controller class GameEngine
     * @param contrl
     *          instance of Controller class Controller
     */
    public SceneImpl(final GameEngine gmEngine, final Controller contrl) {

        this.addKeyListener(this);
        this.controller = contrl;
        GameEngine gameEngine = gmEngine;
        road = new Road(gameEngine);
        Menu menu = new Menu(this.road, gameEngine);
        Market shop = new Market();
        Stats stats = new Stats();

        content.add(road, "road");
        content.add(menu, "menu");
        content.add(shop, "shop");
        content.add(stats, "stats");
        changePanel("menu");

        add(content, BorderLayout.CENTER);
        setSize(WIDTH, HEIGHT);
        setTitle("Late Runner");
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);
        setFocusable(true);
        setFocusTraversalKeysEnabled(false);
        requestFocusInWindow();
        setVisible(true);
    }

    @Override
    public void render() {
        try {
            SwingUtilities.invokeAndWait(this::repaint);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void keyPressed(final KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
            controller.notifyCommand(new MoveRight());
        } else if (e.getKeyCode() == KeyEvent.VK_LEFT) {
            controller.notifyCommand(new MoveLeft());
        }
    }

    @Override
    public void keyTyped(final KeyEvent e) { }

    @Override
    public void keyReleased(final KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
            controller.notifyCommand(new Stop());
        } else if (e.getKeyCode() == KeyEvent.VK_LEFT) {
            controller.notifyCommand(new Stop());
        }
    }

    @Override
    public void setInputController(final Controller c) {
        controller = c;
    }

    @Override
    public Road getRoad() {
        return road;
    }

    /**
     * Switches between panels in order to show the desired one.
     * 
     * @param name
     *          the name of the panel to show
     */
    public static void changePanel(final String name) {
        cl.show(content, name);
    }

    /**
     * Gets the content panel.
     * 
     * @return
     *          content panel previously defined
     */
    public static JPanel getContent() {
        return content;
    }
}
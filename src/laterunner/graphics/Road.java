package laterunner.graphics;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.util.List;

import javax.swing.JLabel;

import laterunner.core.GameEngine;
import laterunner.model.user.User;
import laterunner.model.vehicle.Vehicle;
import laterunner.model.world.GameState;
import laterunner.model.world.World;

/**
 * Road is the class that contains the real game. It displays all the vehicles and its elements are constantly repainted.
 *
 */
public class Road extends PanelImpl implements Runnable {

    private static final long serialVersionUID = 1L;
    private static final float SIZE = 60;
    private static final int STEP = -96;
    private static final int LEFT_CROSS = 372;
    private static final int RIGHT_CROSS = 572;
    private static final int SPEED_FACTOR = 21;
    private static int y = STEP;
    private static JLabel label = new JLabel();
    private Audio audio = new Audio();

    private GameEngine gameEngine;
    private GameState gameState;

    /**
     * Common Road constructor: sets the GameEngine.
     * 
     * @param gmEngine
     *          an instance of GameEngine class
     */
    Road(final GameEngine gmEngine) {
        this.gameEngine = gmEngine;
        Font fontChalkDash = super.createFont("Digital Dot Roadsign.otf", SIZE);
        label.setFont(fontChalkDash);
        label.setForeground(Color.BLACK);
        this.add(label, BorderLayout.NORTH);
    }

    @Override
    protected void paintComponent(final Graphics graphics) {
        super.paintComponent(graphics);

        World scene = gameState.getWorld();
        List<Vehicle> entities = scene.getSceneEntities();
        graphics.drawImage(super.getPics().getImage(Icons.ROAD), 0, 0, null);
        this.updateCross();
        graphics.drawImage(super.getPics().getImage(Icons.CROSS), LEFT_CROSS, y, null);
        graphics.drawImage(super.getPics().getImage(Icons.CROSS), RIGHT_CROSS, y, null);

        entities.forEach((Vehicle vehicle) -> {
            if (this.gameEngine.isSurvival()) {
                label.setText("<html><div style='text-align: center;'>" + "Score: " + gameState.getScore() + "</div></html>");
            } else {
            label.setText("<html><div style='text-align: center;'>" + "Lives: " + User.getUser().getUserLives()
                + "&#160; &#160; &#160; &#160; &#160;" + "Score: " + gameState.getScore() + "</div></html>");
            }

            switch (vehicle.getType()) {
                case MOTORBIKE:
                    drawVehicle(graphics, vehicle, Icons.MOTORBIKE);
                    break;
                case BUS:
                    drawVehicle(graphics, vehicle, Icons.BUS);
                    break;
                case OBSTACLE_CAR:
                    drawVehicle(graphics, vehicle, Icons.JEEP);
                    break;
                case USER_CAR:
                    drawVehicle(graphics, vehicle, Icons.CAR);
                    break;
            }

        });
    }

    @Override
    public void run() {
        gameEngine.mainLoop();
    }

    /**
     * Sets the GameState.
     * @param gmState
     *          an instance of GameState class
     */
    public void setGameState(final GameState gmState) {
        this.gameState = gmState;
    }

    /**
     * Returns an instance of Audio class.
     * @return
     *          an instance of Audio class
     */
    public Audio getAudio() {
        return this.audio;
    }

    private void drawVehicle(final Graphics graphics, final Vehicle vehicle, final Icons icon) {
        graphics.drawImage(super.getPics().getImage(icon), (int) vehicle.getCurrentPosition().getX(),
                (int) vehicle.getCurrentPosition().getY(), null);
    }

    private void updateCross() {
        y += SPEED_FACTOR;
        if (y >= 0) {
            y = STEP;
        }
    }
}